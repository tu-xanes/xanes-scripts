#!/usr/bin/python3

import numpy
import h5py
import epics
import threading
import time
import libs.devices as devices

def run():
    ## data
    datapath = "/EPICS/data"
    #datafilename = "xanes"

    ## event handlers
    eve = threading.Event()
    def onchange(**kwargs):
        if kwargs["value"]>0:
            eve.set()

    ## connect to epics PVs
    print("Connecting to epics PVs...")
    cancelpv = epics.PV("XNS:FWT:1:bo.VAL", auto_monitor=True)
    runpv = epics.PV("XNS:FWT:2:bo.VAL", auto_monitor=True, callback=onchange) 
    rebootpv = epics.PV("XNS:FWT:3:bo.VAL", auto_monitor=True, callback=onchange)
    backgroundpv = epics.PV("XNS:FWT:4:bo.VAL", auto_monitor=True, callback=onchange)
    statuspv = epics.PV("XNS:FWT:1:str.VAL", auto_monitor=False)
    progbarpv = epics.PV("XNS:FWT:2:ao.VAL", auto_monitor=False)
    h5filepv = epics.PV("XNS:FWT:1:wave.VAL", auto_monitor=False)
    busypv = epics.PV("XNS:FWT:5:bo.VAL", auto_monitor=False)
    
    time.sleep(2)
    print("PVs OK")

    ## create writer
    w = devices.Writer()
    w.set_datapath(datapath)

    ## reset function to run after user cancel request
    def reset():
        eve.clear()
        w.close()
        cancelpv.put(0)
        runpv.put(0)
        rebootpv.put(0)
        backgroundpv.put(0)
        busypv.put(0)

    ## create devices
    devicelist = []
    eiger = devices.EIGER()
    motors = devices.MOTORS()
    pressure = devices.PRESSURE()
    valves = devices.VALVES()
    devicelist.append(eiger)
    devicelist.append(motors)
    devicelist.append(pressure)
    devicelist.append(valves)

    ## connect devices
    print(f"[{time.asctime()}] Connecting epics devices...")
    devices.connect(devicelist)
    time.sleep(4)

    ## main loop
    print(f"[{time.asctime()}] Listening...")
    statuspv.put("Ready")
    while(True):
        try:
            reset()
            eve.wait()
            cancelpv.put(0)

            ## end script if reboot is pressed
            if (int(rebootpv.value)): 
                statuspv.put("Rebooting...")
                busypv.put(1)
                print(f"[{time.asctime()}] Rebooting...")
                return

            ## take and set detector background image
            if (int(backgroundpv.value)):
                statuspv.put("Set detector background...")
                busypv.put(1)
                err = eiger.set_det_background()
                if err:
                    statuspv.put("Background error. Check logs.")
                else:
                    statuspv.put("Background set OK")
                continue

            ## update status
            statuspv.put("Acquisition started")
            busypv.put(1)
            progbarpv.put(0)

            ## create new file
            datafilename = epics.caget("XNS:FWT:2:str.VAL", as_string=True)
            w.newfile(datafilename)
            h5filepv.put(w.filename)

            ## create datasets
            devices.createdatasets(devicelist, w)

            ## capture parameters
            ## input values for branch A
            images = int(epics.caget("XNS:FWT:1:ao.VAL"))
            exposure = int(epics.caget("XNS:FWT:3:ao.VAL"))

            ## save parameters
            w.save_dataset("parameters/timeStart", time.asctime())
            w.save_dataset("parameters/Images", images)
            w.save_dataset("parameters/Exposure", exposure)
            w.save_dataset("parameters/Sample", epics.caget("XNS:FWT:2:wave.VAL", as_string=True))

            #progcounter
            progtotal = images
            progcounter = 0

            ## setup detector
            err = eiger.waitdetfree(30.0)
            if err:
                statuspv.put("Detector in busy state for too long")
                reset()
                continue

            # update status
            statuspv.put("Acquiring: {}/{}, {:.1f}%".format(0,images,0.0))

            # start detector
            eiger.set_numimages(images)
            eiger.set_exposure(exposure)
            time.sleep(0.5)
            eiger.acquire()

            ## main acquisition loop
            for i in range(images):
                if cancelpv.value>0:
                    statuspv.put("Acquisition aborted")
                    eiger.stop()
                    reset()
                    break
                err = eiger.waitnewimg(exposure+5.0)
                if err:
                    statuspv.put("Error waiting for new ccd image")
                    break

                ## append data to file
                devices.appenddata(devicelist, w)

                # update status
                prog = (100.0*(i+1)/images)
                statuspv.put("Acquiring {}/{}, {:.1f}%".format(i+1,images,prog))
                progcounter+=1
                progbarpv.put((100.0*(progcounter/progtotal)))

                ## end of acquisition loop

            ## take devices snapshot
            devices.snapshot(devicelist, w)

            ## save end time
            w.save_dataset("parameters/timeEnd", time.asctime())

            ## create hard links
            eiger.createlinks(w)

            ## close file
            w.close()

            ## update status
            statuspv.put("Acquisition done")
            reset()

        except Exception as err:
            print(f"\n[{time.asctime()}] error exception:")
            print(err)
            w.close()
            reset()
            time.sleep(1)

    print(f"[{time.asctime()}] End of fwriter script")

## main function call
print(f"\n[{time.asctime()}] Starting FWriter...")
run()
