from libs.basedev import BASEDEV
import epics
import numpy as np
import time
import threading

class EIGER(BASEDEV):
    def __init__(self, **kwargs):
        self.desc = "Dectris Eiger detector"
        self.eve = threading.Event()
        self.eveimg = threading.Event()
        self.evefree = threading.Event()
        self.currentbackground = None
        self.validbackground = False
    
    def connect(self, **kwargs):
        ## Event flag handlers
        def onchange(**kwargs):
            self.eve.set()
        def onchangeimg(**kwargs):
            self.eveimg.set()
        def onchangefree(**kwargs):
            if kwargs["value"]==0:
                self.evefree.set()
        ## PVs
        self.acqpv = epics.PV("XNS:EIGER:cam1:Acquire", auto_monitor=False)
        self.statepv = epics.PV("XNS:EIGER:cam1:DetectorState_RBV", auto_monitor=True)
        self.idpv = epics.PV("XNS:EIGER:cam1:ArrayCounter_RBV", auto_monitor=True, callback=onchange)
        self.fullimagepv = epics.PV("XNS:EIGER:image2:ArrayData", auto_monitor=True, callback=onchangeimg)
        self.detfreepv = epics.PV("XNS:EIGER:cam1:AcquireBusy", auto_monitor=True, callback=onchangefree)
        self.fullrawimagepv = epics.PV(f"XNS:EIGER:image1:ArrayData", auto_monitor=True)
        # self.temppv = epics.PV("XNS:EIGER:cam1:Temp0_RBV", auto_monitor=True)        
        self.specpv = epics.PV("XNS:EIGER:spectrum_RBV", auto_monitor=True)
        self.totalsumpv = epics.PV("XNS:EIGER:Stats2:Total_RBV", auto_monitor=True)

    def snapshot(self, **kwargs):
        w = kwargs["w"]
        xdim = epics.caget("XNS:EIGER:image1:ArraySize0_RBV")
        ydim = epics.caget("XNS:EIGER:image1:ArraySize1_RBV")
        w.save_dataset('instruments/eiger/description', epics.caget("XNS:EIGER:cam1:Description_RBV", as_string=True))
        w.save_dataset('instruments/eiger/xdim', xdim)
        w.save_dataset('instruments/eiger/ydim', ydim)
        w.save_dataset('instruments/eiger/exposure', epics.caget("XNS:EIGER:cam1:AcquireTime"))
        w.save_dataset('instruments/eiger/Threshold1', epics.caget("XNS:EIGER:cam1:ThresholdEnergy_RBV"))
        w.save_dataset('instruments/eiger/Threshold1_status', epics.caget("XNS:EIGER:cam1:Threshold1Enable_RBV", as_string=True))
        w.save_dataset('instruments/eiger/Threshold2', epics.caget("XNS:EIGER:cam1:Threshold2Energy_RBV"))
        w.save_dataset('instruments/eiger/Threshold2_status', epics.caget("XNS:EIGER:cam1:Threshold2Enable_RBV", as_string=True))
        w.save_dataset('instruments/eiger/Threshold_Diff_status', epics.caget("XNS:EIGER:cam1:ThresholdDiffEnable_RBV", as_string=True))
        w.save_dataset('instruments/eiger/raw/temperature', epics.caget("XNS:EIGER:cam1:Temp0_RBV"))
        if self.validbackground:
            w.save_dataset('instruments/eiger/raw/background', self.currentbackground)

    def createdatasets(self, **kwargs):
        w = kwargs["w"]
        # full images
        xdim = epics.caget("XNS:EIGER:image1:ArraySize0_RBV")
        ydim = epics.caget("XNS:EIGER:image1:ArraySize1_RBV")
        self.fulldim = (ydim,xdim)
        self.fullrimgdpath = "instruments/eiger/raw/raw_images"
        w.create_dataset(self.fullrimgdpath, dtype=np.uint32, dimensions=self.fulldim, compression=True)
        self.fullimgdpath = "instruments/eiger/raw/images"
        w.create_dataset(self.fullimgdpath, dtype=np.uint32, dimensions=self.fulldim, compression=True)
        self.detiddpath = "instruments/eiger/raw/ids"
        w.create_dataset(self.detiddpath, dtype=np.float64)
        # self.tempdpath = "instruments/eiger/raw/temperature"
        # w.create_dataset(self.tempdpath, dtype=np.float64)
        # spectra
        self.specdpath = "instruments/eiger/processed/spectra"
        w.create_dataset(self.specdpath, dtype=np.float64, dimensions=xdim)
        self.totalsumdpath = "instruments/eiger/processed/total_sum"
        w.create_dataset(self.totalsumdpath, dtype=np.float64)

    def createlinks(self, w):
        w.save_hardlink("data/images", self.fullimgdpath)
        ## w.save_hardlink("data/spectra", self.specdpath)
    
    def appenddata(self, **kwargs):
        w = kwargs["w"]
        # images
        w.append_dataset(self.fullrimgdpath, np.reshape(self.tou32(self.fullrawimagepv.get()), self.fulldim))
        w.append_dataset(self.fullimgdpath, np.reshape(self.tou32(self.fullimagepv.get()), self.fulldim))
        # detector ids
        w.append_dataset(self.detiddpath, self.idpv.get())
        # temperature
        # w.append_dataset(self.tempdpath, self.temppv.get())
        # spectra
        w.append_dataset(self.specdpath, self.specpv.get())
        w.append_dataset(self.totalsumdpath, self.totalsumpv.get())

    def set_exposure(self, exposure):
        ## sets exposure using one of the motions sequence records to match exposure and period
        epics.caput("XNS:EIGER:cam1:AcquireTime", exposure)
        time.sleep(0.5)
        epics.caput("XNS:EIGER:cam1:AcquirePeriod", exposure)

    def get_exposure(self):
        return epics.caget("XNS:EIGER:cam1:AcquireTime")

    def set_numimages(self, num):
        epics.caput(f"XNS:EIGER:cam1:NumImages", num)

    def waitnewid(self, timeout):
        self.eve.clear()
        err = not self.eve.wait(timeout)
        self.eve.clear()
        return err

    def tou32(self, inarray):
        return np.array(inarray, dtype=np.uint32)
    
    def acquire(self):
        self.acqpv.put(1)

    def stop(self):
        self.acqpv.put(0)
    
    def waitnewimg(self, timeout):
        self.eveimg.clear()
        err = not self.eveimg.wait(timeout)
        self.eveimg.clear()
        return err
    
    def waitdetfree(self, timeout):
        if int(self.detfreepv.value)==0:
            err = False
        else:
            self.stop()
            self.evefree.clear()
            err = not self.evefree.wait(timeout)
            self.evefree.clear()
        return err

    def set_det_background(self):
        err = self.waitdetfree(30.0)
        if err: 
            print(f"[{time.asctime()}] timeout waiting for detector to stop current acquisition")
            return err

        ## setup detector
        exposure = self.get_exposure()
        self.set_exposure(0.1)
        self.set_numimages(1)
        epics.caput("XNS:EIGER:Proc1:EnableBackground", 0)
        time.sleep(0.5)

        ## take one image        
        self.acquire()
        err = self.waitnewimg(10.0)
        if err: 
            print(f"[{time.asctime()}] timeout waiting for new detector image")
            return err

        ## save background
        epics.caput("XNS:EIGER:Proc1:SaveBackground", 1)
        xdim = epics.caget("XNS:EIGER:image1:ArraySize0_RBV")
        ydim = epics.caget("XNS:EIGER:image1:ArraySize1_RBV")
        self.fulldim = (ydim,xdim)
        self.currentbackground = np.reshape(self.tou32(self.fullrawimagepv.get()), self.fulldim)
        self.validbackground = True

        ## enable background
        epics.caput("XNS:EIGER:Proc1:EnableBackground", 1)

        self.set_exposure(exposure)

        return False

