from libs.basedev import BASEDEV
import epics
import numpy as np
import time
import threading

class PRESSURE(BASEDEV):
    def __init__(self, **kwargs):
        self.desc = "Pressure gauges"

    def connect(self, **kwargs):
        self.s1pv = epics.PV("XNS:tpg:S1Measure", auto_monitor=True)
        self.s2pv = epics.PV("XNS:tpg:S2Measure", auto_monitor=True)
        self.s3pv = epics.PV("XNS:tpg:S3Measure", auto_monitor=True)
    
    def createdatasets(self, **kwargs):
        w = kwargs["w"]
        w.create_dataset("instruments/pressure/Spectrometer", dtype=np.float64)
        w.create_dataset("instruments/pressure/Load_Lock", dtype=np.float64)
        w.create_dataset("instruments/pressure/Source", dtype=np.float64)

    def appenddata(self, **kwargs):
        w = kwargs["w"]
        w.append_dataset("instruments/pressure/Spectrometer", self.s1pv.get())
        w.append_dataset("instruments/pressure/Load_Lock", self.s2pv.get())
        w.append_dataset("instruments/pressure/Source", self.s3pv.get())