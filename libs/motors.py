from libs.basedev import BASEDEV
import epics
import numpy as np
import time
import threading

class MOTORS(BASEDEV):
    def __init__(self, **kwargs):
        self.desc = "Motor positions"
    
    def snapshot(self, **kwargs):
        w = kwargs["w"]

        ## actual raw motor positions
        w.save_dataset('instruments/motors/raw/m1', epics.caget("XNS:PHY:m1.RBV"))
        w.save_dataset('instruments/motors/raw/m1_desc', epics.caget("XNS:PHY:m1.DESC"))
        w.save_dataset('instruments/motors/raw/m2', epics.caget("XNS:PHY:m2.RBV"))
        w.save_dataset('instruments/motors/raw/m2_desc', epics.caget("XNS:PHY:m2.DESC"))
        w.save_dataset('instruments/motors/raw/m3', epics.caget("XNS:PHY:m3.RBV"))
        w.save_dataset('instruments/motors/raw/m3_desc', epics.caget("XNS:PHY:m3.DESC"))
        w.save_dataset('instruments/motors/raw/m4', epics.caget("XNS:PHY:m4.RBV"))
        w.save_dataset('instruments/motors/raw/m4_desc', epics.caget("XNS:PHY:m4.DESC"))
        w.save_dataset('instruments/motors/raw/m5', epics.caget("XNS:PHY:m5.RBV"))
        w.save_dataset('instruments/motors/raw/m5_desc', epics.caget("XNS:PHY:m5.DESC"))
        w.save_dataset('instruments/motors/raw/m6', epics.caget("XNS:PHY:m6.RBV"))
        w.save_dataset('instruments/motors/raw/m6_desc', epics.caget("XNS:PHY:m6.DESC"))
        w.save_dataset('instruments/motors/raw/m7', epics.caget("XNS:PHY:m7.RBV"))
        w.save_dataset('instruments/motors/raw/m7_desc', epics.caget("XNS:PHY:m7.DESC"))
        w.save_dataset('instruments/motors/raw/m8', epics.caget("XNS:PHY:m8.RBV"))
        w.save_dataset('instruments/motors/raw/m8_desc', epics.caget("XNS:PHY:m8.DESC"))
        w.save_dataset('instruments/motors/raw/m9', epics.caget("XNS:PHY:m9.RBV"))
        w.save_dataset('instruments/motors/raw/m9_desc', epics.caget("XNS:PHY:m9.DESC"))

        ## virtual motor positions
        w.save_dataset('instruments/motors/virtual/Filter', epics.caget("XNS:MTN:m8:transf1.D"))
        w.save_dataset('instruments/motors/virtual/Filter_Selection', epics.caget("XNS:MTN:filters:mselect", as_string=True))
        w.save_dataset('instruments/motors/virtual/Crystal_Y', epics.caget("XNS:MTN:m1:transf1.D"))
        w.save_dataset('instruments/motors/virtual/Crystal_Rotation', epics.caget("XNS:MTN:m2:transf1.D"))
        w.save_dataset('instruments/motors/virtual/Crystal_Selection', epics.caget("XNS:MTN:xtal:mselect", as_string=True))
        w.save_dataset('instruments/motors/virtual/Crystal_Tilt', epics.caget("XNS:MTN:m3:transf1.D"))
        w.save_dataset('instruments/motors/virtual/Detector_Rotation', epics.caget("XNS:MTN:m7:transf1.D"))
        w.save_dataset('instruments/motors/virtual/Detector_X_Offset', epics.caget("XNS:MTN:Det-off-X"))
        w.save_dataset('instruments/motors/virtual/Detector_Y_Offset', epics.caget("XNS:MTN:Det-off-Y"))
        w.save_dataset('instruments/motors/virtual/Detector_Long', epics.caget("XNS:MTN:m5:transf1.D"))
        w.save_dataset('instruments/motors/virtual/Detector_Short', epics.caget("XNS:MTN:m6:transf1.D"))
        w.save_dataset('instruments/motors/virtual/Theta', epics.caget("XNS:MTN:theta"))
        w.save_dataset('instruments/motors/virtual/Virtual_Theta', epics.caget("XNS:PHY:m12.RBV"))

