from libs.basedev import BASEDEV
import epics
import numpy as np
import time
import threading

class VALVES(BASEDEV):
    def __init__(self, **kwargs):
        self.desc = "Vacuum Valves"

    def snapshot(self, **kwargs):
        w = kwargs["w"]

        ## Valves
        w.save_dataset('instruments/valves/V1', epics.caget("XNS:VAC:outputs:mbbi1.B0"))
        w.save_dataset('instruments/valves/V1_DESC', epics.caget("XNS:VAC:extra:labels3:B0"))
        w.save_dataset('instruments/valves/V2', epics.caget("XNS:VAC:outputs:mbbi1.B1"))
        w.save_dataset('instruments/valves/V2_DESC', epics.caget("XNS:VAC:extra:labels3:B1"))
        w.save_dataset('instruments/valves/V3', epics.caget("XNS:VAC:outputs:mbbi1.B2"))
        w.save_dataset('instruments/valves/V3_DESC', epics.caget("XNS:VAC:extra:labels3:B2"))
        w.save_dataset('instruments/valves/V4', epics.caget("XNS:VAC:outputs:mbbi1.B3"))
        w.save_dataset('instruments/valves/V4_DESC', epics.caget("XNS:VAC:extra:labels3:B3"))
        w.save_dataset('instruments/valves/V5', epics.caget("XNS:VAC:outputs:mbbi1.B5"))
        w.save_dataset('instruments/valves/V5_DESC', epics.caget("XNS:VAC:extra:labels3:B4"))

        ## Switches
        w.save_dataset('instruments/valves/LL_Door_closed', epics.caget("XNS:VAC:inputs:mbbi2.B3"))
        w.save_dataset('instruments/valves/Sample_Stick_isOut', epics.caget("XNS:VAC:inputs:mbbi1.BA"))