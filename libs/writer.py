## writer class
import h5py
import time
import pathlib

class Writer:
    def __init__(self):
        self.filename = ""
        self.datapath = "."
        self.path = ""
        self.fullpath = ""
        self.fobj = None
        self.dsdict = {}

    def set_datapath(self, path):
        self.datapath=path

    def newfile(*args):
        self = args[0]
        if len(args)>1:
            extra = "_"+str(args[1])
        else:
            extra = ""
        if self.fobj != None:
            self.fobj.close()
        n=time.localtime()
        #fpath = "{:04d}/{:02d}/{:02d}".format(n.tm_year,n.tm_mon,n.tm_mday)
        fpath = "{:04d}/{:02d}".format(n.tm_year,n.tm_mon)
        self.path = f"{self.datapath}/{fpath}"
        pathlib.Path(self.path).mkdir(parents=True,exist_ok=True)
        self.filename = "{:04d}{:02d}{:02d}_{:02d}{:02d}{:02d}{}.h5".format(n.tm_year,n.tm_mon,n.tm_mday,n.tm_hour,n.tm_min,n.tm_sec,extra)
        self.fullpath = f"{self.path}/{self.filename}"
        self.fobj = h5py.File(self.fullpath, 'w')

    def save_dataset(self, path, data):
        self.fobj[path]=data

    def save_softlink(self, path, datapath):
        self.fobj[path]=h5py.SoftLink(datapath)

    def save_hardlink(self, path, datapath):
        self.fobj[path]=self.fobj[datapath]

    def create_dataset(self, path, dtype, dimensions=None, compression=False):
        if dimensions==None:
            dimensions=(1,)
            compression=False
        elif type(dimensions)==type(0):
            dimensions=(dimensions,)
        shape = (0,)+dimensions
        maxshape = (None,)+dimensions
        chunks = (1,)+dimensions
        if compression:
            cp = 'gzip'
            sf = True
        else:
            cp = None
            sf = False
        self.dsdict[path]= self.fobj.create_dataset(path, shape=shape, dtype=dtype, maxshape=maxshape, chunks=chunks, compression=cp,shuffle=sf)

    def append_dataset(self, path, data):
        ds = self.dsdict[path]
        nshape = (ds.shape[0]+1,)+ds.shape[1:]
        self.dsdict[path].resize(nshape)
        ds[-1]=data

    def close(self):
        if self.fobj != None:
            self.fobj.close()
            self.fobk = None

    def __del__(self):
        if self.fobj != None:
            self.fobj.close()
            self.fobj = None
