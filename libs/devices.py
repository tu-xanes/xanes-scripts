from libs.writer import Writer
from libs.eiger import EIGER
from libs.motors import MOTORS
from libs.pressure import PRESSURE
from libs.valves import VALVES

def connect(devlist):
    for d in devlist:
        d.connect()

def createdatasets(devlist, w):
    for d in devlist:
        d.createdatasets(w=w)

def appenddata(devlist, w):
    for d in devlist:
        d.appenddata(w=w)

def snapshot(devlist, w):
    for d in devlist:
        d.snapshot(w=w)
